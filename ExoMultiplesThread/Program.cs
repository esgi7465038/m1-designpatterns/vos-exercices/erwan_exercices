﻿using System;
using System.Threading;
using System.Timers;

namespace ExoMultiplesThreads
{
    class Program
    {
        private static System.Timers.Timer timer;
        private static MyThreadLetter myThreadLetter;

        static void Main(string[] args)
        {
            myThreadLetter = new MyThreadLetter();
            myThreadLetter.Start();
            Console.WriteLine("Thread start !");

            timer = new System.Timers.Timer(1000);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Enabled = true;
            Console.WriteLine("Main thread: sleeping 10s...");
            Thread.Sleep(10000);
            myThreadLetter.Stop = true;
            myThreadLetter.Join();
            Console.WriteLine("Main thread stop !");
            timer.Stop();
            timer.Dispose();
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine();
        }

        private static void OnTimedEvent(Object source, ElapsedEventArgs e)
        {
            Console.WriteLine("Timer: {0:HH:mm:ss.fff}", e.SignalTime);
        }
    }
}