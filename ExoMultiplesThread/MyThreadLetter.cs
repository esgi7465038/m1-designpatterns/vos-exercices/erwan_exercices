﻿using System;
using System.Diagnostics;
using System.Threading;

namespace ExoMultiplesThreads
{
    class MyThreadLetter
    {
        private readonly Random rnd = new();
        private volatile bool stop = false;
        public bool Stop
        {
            get { return stop; }
            set { stop = value; }
        }

        public char Letter { get; private set; } = 'a';

        private Thread LaTache;

        public MyThreadLetter()
        {
            LaTache = new Thread(new ThreadStart(RunLetter));
        }

        public void Start()
        {
            LaTache.Start();
            Console.WriteLine("Thread Lettre started");
        }

        public void Join()
        {
            LaTache.Join();
        }

        public void RunLetter()
        {
            var sw = Stopwatch.StartNew();

            Console.WriteLine("Thread start");
            Console.WriteLine("\t...RunLetter: start, id {0}, state {1}, prio {2}", Thread.CurrentThread.ManagedThreadId, Thread.CurrentThread.ThreadState, Thread.CurrentThread.Priority);
            while (!stop)
            {
                if (Letter < 'z')
                {
                    Letter++;
                }
                else
                {
                    Letter = 'a';
                }
                Console.WriteLine("Thread Lettre: {0} - {1}", Letter, sw.ElapsedMilliseconds);
                Thread.Sleep(rnd.Next(50, 1267));
            }
        }
    }
}