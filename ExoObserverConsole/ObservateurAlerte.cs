using ExerciceObserverConsole;
using Observateur;

namespace ExoObserverConsole
{
    internal class ObservateurAlerte : IObservateur
    {
        public void Actualiser(ISujet sujet)
        {
            var observedValue = ( (SujetConcret) sujet ).observedValue;

            if (observedValue > 90)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("ALERT");
                Console.ResetColor();
            }
            else if (observedValue > 75)
            {
                Console.ForegroundColor = ConsoleColor.DarkYellow;
                Console.WriteLine("Warn");
            }

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine($"[{GetHashCode()}]: {observedValue}");
            Console.ResetColor();
        }
    }
}