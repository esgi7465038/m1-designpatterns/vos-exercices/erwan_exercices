﻿namespace Feutres
{
    internal class Feutre
    {
        private string matiere;
        public string Matiere => matiere;

        private ConsoleColor couleur;
        public ConsoleColor Couleur
        {
            get => couleur;
            set => couleur = value;
        }

        public bool EstBouche { get; private set; }

        public Feutre()
        {
            this.matiere = "Métal";
            this.Couleur = ConsoleColor.Blue;
            this.EstBouche = true;
        }

        public Feutre(string matiere,
                     ConsoleColor couleur,
                     bool estBouche = true)
        {
            this.matiere = matiere;
            this.Couleur = couleur;
            this.EstBouche = estBouche;
        }
        public bool Debouche()
        {
            EstBouche = false;
            return EstBouche;
        }

        public bool Bouche()
        {
            EstBouche = true;
            return EstBouche;
        }

        public bool Ecrire(string message)
        {
            if (EstBouche == true)
            {
                Console.WriteLine("Impossible d'écrire, le stylo est bouché");
                return false;
            }
            else
            {
                ConsoleColor couleurActuelle = Console.ForegroundColor;
                Console.ForegroundColor = couleur;
                Console.WriteLine(message);
                Console.ForegroundColor = couleurActuelle;
                return true;
            }
        }
    }
}
