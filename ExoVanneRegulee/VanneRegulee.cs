﻿namespace Vannes;

public class VanneRegulee : Vanne, IRegulable
{
    public bool IsReguleOn { get; set; }

    public void Regule(int throughput)
    {
        Position = ( throughput <= maxThroughput ) ? ( throughput * 100 / maxThroughput ) : 100;
    }

    public VanneRegulee(bool isReguleOn = true)
    {
        IsReguleOn = isReguleOn;
    }
}
